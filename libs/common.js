"use strict";

const co = require('co');
const Request = require('koa-request');

let common = {};

/**
 * 对前端输出弹框（最好不用，前端自行处理）
 * @param {string} errMsg 
 * @param {string} redirection 
 * @param {object} res 
 */
common.trigger = (errMsg, redirection, res) => {
    res.body = '<script>alert("' + errMsg + '");window.location.href="' + redirection + '"</script>';
};

/**
 * 判断客户端（浏览器）是否是移动端
 * @param {object} ctx 
 */
common.isMobile = (ctx) => {
    let userAgent = ctx.header['user-agent'];

    return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(userAgent));
};

/**
 * 判断客户端（浏览器）语言的中间件，并在上下文中设置
 * @param {*} next 
 */
common.checkLang = function*(next) {
    let xForwardFor = this.request.header['x-forwarded-for'];
    if (xForwardFor) {
        let ip  = xForwardFor.split(',')[0];
        let url = `http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json&ip=${ip}`;
        // let result = yield Request.get({
        //     url: url
        // });
        // let {country, province} = JSON.parse(result.body);
        this.country =  '中国';
        this.province = '上海';
        yield next;
    } else {
        this.country = '中国';
        yield next;
    }
}

/**
 * 获取给定手机号的供应商：中国移动、中国联通、中国电信
 * @param {string} phone 
 */
common.phoneSupplier = function(phone) {
    const chinaMobilePrefixes = ['134', '135', '136', '137', '138', '139', '150', '151', '152', '157', '158', '159', '182', '183', '184', '187', '188', '1705', '178', '147'];
    const ChinaUnicomPrefixes = ['130', '131', '132', '155', '156', '171', '175', '185', '186', '1707', '1708', '1709', '176', '145'];
    const ChinaTelecomPrefixes = ['133', '1349', '153', '180', '181', '189', '1700', '1701', '173', '177'];
    
    const chinaMobileMatch = chinaMobilePrefixes.filter(val => phone.indexOf(val) === 0);
    const chinaUnicomMatch = ChinaUnicomPrefixes.filter(val => phone.indexOf(val) === 0);
    const chinaTelecomMatch = ChinaTelecomPrefixes.filter(val => phone.indexOf(val) === 0);

    if (chinaMobileMatch.length > 0) {
        return '中国移动';
    }
    if (chinaUnicomMatch.length > 0) {
        return '中国联通';
    }
    if (chinaTelecomMatch.length > 0) {
        return '中国电信';
    }
}

/**
 * 把数字转化为每三位用逗号分隔的方式：1234567 -> 1,234,567
 * @param {*} x 
 */
common.numberWithCommas = function(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

module.exports = common;