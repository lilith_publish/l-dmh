'use strict';

const router = require('koa-router')();
//const co = require('co');
//const random = require('random-gen');
//const Request = require('koa-request');
//const urlencode = require('urlencode');
//const logger = require('pedograph').logger;
//const date = require('../../libs/date');
const isMobile = require('../../libs/common').isMobile;
//const commondb = require('../commonmodels');
//const db = require('../models');
//const wechatApi = require('../../libs/wechatApi');
//const fs =require('fs');
//const path = require('path');
//首页
router.get('/', function*() {
  if(this.country !== 'CN' && this.country !== '中国') {
      this.redirect('/en');
  }else{
    this.redirect('/cn');
  }                                                                    
});

router.get('/cn', function* () {
  const csrf = this.csrf;
  const jadeFile = isMobile(this) ? 'home_m' : 'home';

  this.render(jadeFile, {
    csrf,
  });

});

//英文                                                                                                                                                          
router.get('/en', function* () {
  const csrf = this.csrf;
  const jadeFile = isMobile(this) ? 'home_m_en' : 'home_en';
  this.render(jadeFile, {
    csrf,
  });

});

module.exports = app => app.use(router.routes());