"use strict";

const Sequelize = require('sequelize');
const config    = require('../../config/config');
const fs        = require('fs');
const path      = require('path');
let db          = {};

var sequelize = new Sequelize(config.commondb, {
    logging: false,
    define : {
        freezeTableName: true,
        timestamps: true,
        charset: 'utf8',
        collate: 'utf8_general_ci'
    },
    pool: {
        logging: true,
        max: 800,
        min: 0,
        idle: 10000
    }
});

fs.readdirSync(__dirname)
    .filter(file => (file.indexOf('.') !== 0) && (file !== 'index.js'))
    .forEach(file => {
        let model      = sequelize['import'](path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
    if ('associate' in db[modelName]) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;

