$(function () {
    $('.modal-close').click(function () {
        $('.top_box_video').attr('src', '');
        $(this).parent().hide().parent().hide();
    });
    $('.btn-player').click(function () {
        $('.modal,.modal-video').show();
        var e = $(this).data("src");
        $('.top_box_video').attr('src', e);
    });
});