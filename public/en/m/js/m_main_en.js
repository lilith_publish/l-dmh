// mp4 player
!function(a) {
    a.playVideo = function(i) {
        function o() {
            var a = c.height()
              , i = c.width();
            i > 970 && (p.css("top", .5 * (a - 550) + "px"),
            p.css("left", .5 * (i - 970) + "px"))
        }
        function e() {
            p.animate({
                opacity: 0
            }, 300, function() {
                p.remove(),
                s.animate({
                    opacity: 0
                }, 300, function() {
                    s.remove(),
                    n.removeClass("noscroll")
                })
            })
        }
        var l = i.src
          , n = a("body")
          , c = a(window)
          , s = a('<div class="player-dark-bg"></div>');
        if (c.width() > 970)
            var d = null
              , p = a('<div class="player-wrap"></div>')
              , t = a('<div class="player-container"></div>');
        else
            var d = a('<div class="close-player"></div>')
              , p = a('<div class="player-wrap-mobile"></div>')
              , t = a('<div class="player-container-mobile"></div>');
        a('<video preload="metadata" id="popvideo" controls="controls" ><source src="' + l + '" type="video/mp4"></source></video> ').appendTo(t),
        t.appendTo(p),
        d && p.append(d),
        o(),
        c.resize(o),
       setTimeout(function(){ s.bind("click", e),document.getElementById('popvideo').load();document.getElementById('popvideo').play()},500)
        null !== d && d.bind("click", e),
        function() {
            n.addClass("noscroll"),
            s.appendTo(n).css("display", "block"),
            s.animate({
                opacity: .85
            }, 300, function() {
                p.appendTo(n).css("display", "block"),
                p.animate({
                    opacity: 1
                }, 300, function() {})
            });
        }()
    }
}(jQuery);
$(function () {
    $('.btn-player').click(function () {
        var e = $(this).data("src");
        $.playVideo({
            src: e
        })
    });  
});