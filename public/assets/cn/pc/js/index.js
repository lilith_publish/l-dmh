$(function(){
    var startVideo = document.getElementById('start-video');
    startVideo.oncanplay = function() {
        $('.loading').hide();
    }
    startVideo.onended = function() {
        $('.start-container').hide();
        $('.main-container').show();
    };
    $('.btn-mute').click(function () {
        $('.btn-mute').toggleClass('active');
        if ($(this).hasClass('active')) {
            $('#audio')[0].pause();
        }
        else {
            $('#audio')[0].play();

        }
    });
});
(function(window) {
    function forceSafariPlayAudio() {
        audioEl.play();
    }
    var audioEl = document.getElementById('audio');
    window.addEventListener('touchstart', forceSafariPlayAudio, false);
    window.addEventListener('click', forceSafariPlayAudio, false);
    audioEl.src = '//lilithvideo.lilithcdn.com/dmh/dmh.mp3';
})(window);