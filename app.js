"use strict";

const koa        = require('koa');
const glob       = require('glob');
//const router     = require('koa-router');
const Jade       = require('koa-jade');
const serve      = require('koa-static');
//const path       = require('path');
const co         = require('co');
const bodyParser = require('koa-bodyparser');
const session    = require('koa-session');
const csrf       = require('koa-csrf');
const pedo       = require('pedograph');
const config     = require('./config/config');
//const db         = require('./app/models');
const common     = require('./libs/common');

const app  = koa();
const jade = new Jade({
    viewPath: './app/views',
    app     : app
});

app.keys = config.app.keys;
app.use(serve('./public', {
    maxage: 7 * 24 * 60 * 60 * 1000
}));
app.use(bodyParser());
app.use(session(app));
app.use(common.checkLang);
app.use(pedo.pedograph());
csrf(app, {});

glob.sync('./app/controllers/*.js').forEach(controller => require(controller)(app));

app.use(function *(next) {
    try {
        yield next;
    } catch (err) {
        this.status = err.status || 500;
        this.render('error', {
            error: err
        })
    }
});

co(function* () {
    //yield db.sequelize.sync();
    let server     = app.listen(config.port);
    server.timeout = config.app.timeout;
    pedo.logger.info('listen on port ' + config.port);
});
