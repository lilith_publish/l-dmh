'use strict';

const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const css = require('gulp-mini-css');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const concat = require('gulp-concat');
const postcss = require('gulp-postcss');
const px2rem = require('postcss-px2rem');
const modifyCssUrls = require('gulp-modify-css-urls');
const oss = require('./gulp-aliyun-oss');
const replaceUtil = require('gulp-replace');

function exclude(cssArr) {
  if (!cssArr) cssArr = [];
  if (!Array.isArray(cssArr)) cssArr = [cssArr];
  return [...cssArr].map(val => `!${val}`);
}

// 普通css
const commonCssMap = [ {
  src: [ './public/assets/css/*.css', ...exclude() ],
  dest: './public/css/',
}, {
  src: [ './public/m/css/*.css', ...exclude('./public/m/css/*.min.css') ],
  dest: './public/m/css/',
} ];

// 需要rem转换的css
const remCssMap = [ {
  src: [ './public/assets/css/landing_cbt3_*.css', './public/assets/css/order.css', './public/assets/css/peiyin.css', './public/assets/css/zhubo.css', './public/assets/css/*_m.css', ...exclude() ],
  dest: './public/css/'
} ];

// 需要合并的css
const mergeCssMap = [ {
  src: [ './public/css/hero_m.min.css', './public/css/_wiki_footer_m.min.css', ...exclude() ],
  concat: 'hero_m.min.css',
  dest: './public/css/',
}, {
  src: [ './public/css/nuoer_m.min.css', './public/css/_wiki_footer_m.min.css', ...exclude() ],
  concat: 'nuoer_m.min.css',
  dest: './public/css/',
} ];

// js
const jsMap = [ {
  src: [ './public/assets/js/*.js' ],
  dest: './public/js/',
}, {
  src: [ './public/m/js/*.js', '!./public/m/js/*.min.js' ],
  dest: './public/m/js/',
} ];

const commonCssTasks = [];
for (let i = 0; i < commonCssMap.length; i++) {
  const p = commonCssMap[i];
  gulp.task(`commonCss-${i}`, () => {
    return gulp.src(p.src)
      .pipe(autoprefixer({
        browsers: [ '> 1%' ],
        cascade: false,
      }))
      .pipe(css({
        ext: '.min.css',
      }))
      .pipe(gulp.dest(p.dest));
  });
  commonCssTasks.push(`commonCss-${i}`);
}

const remCssTasks = [];
const plugins = [
  px2rem({
    remUnit: 75
  })
];
for (let i = 0; i < remCssMap.length; i++) {
  const p = remCssMap[i];
  gulp.task(`remCss-${i}`, [...commonCssTasks], () => {
    return gulp.src(p.src)
      .pipe(postcss(plugins))
      .pipe(modifyCssUrls({
        modify(url, filePath) {
          if (url.indexOf('img_local') > -1 || url.indexOf('../font') > -1) {
            return url;
          } else {
            return /(http|https)/.test(url) ? url : `https://lilithimage.lilithcdn.com/aoc/${url.replace('../img/', '')}`;
          }
        },
      }))
      .pipe(replaceUtil('pcpx', 'px'))
      .pipe(autoprefixer({
        browsers: [ '> 1%' ],
        cascade: false,
      }))
      .pipe(css({
        ext: '.min.css',
      }))
      .pipe(gulp.dest(p.dest));
  });
  remCssTasks.push(`remCss-${i}`);
}

const mergeCssTasks = [];
for (let i = 0; i < mergeCssMap.length; i++) {
  const p = mergeCssMap[i];
  gulp.task(`mergeCss-${i}`, [...remCssTasks], () => {
    return gulp.src(p.src)
      .pipe(concat(p.concat))
      .pipe(gulp.dest(p.dest));
  });
  mergeCssTasks.push(`mergeCss-${i}`);
}

const jsTasks = [];
for (var i = 0; i < jsMap.length; i++) {
  const p = jsMap[i];
  gulp.task(`js-${i}`, () => {
    return gulp.src(p.src)
      .pipe(uglify())
      .pipe(rename({
        suffix: '.min',
      }))
      .pipe(gulp.dest(p.dest));
  });
  jsTasks.push(`js-${i}`);
}

// 上传/public/img目录下的图片文件到oss
gulp.task('oss', () => {
  const options = {
    accessKeyId: 'LTAIaIAYpvofCzsY',
    accessKeySecret: 'hACXjkAsmwko4qz0E022PZzGyPkhdF',
    region: 'oss-cn-shanghai',
    bucket: 'lilithimage',
    prefix: 'dmh'
  };
  return gulp.src('./public/en/m/imgs')
    .pipe(oss(options));
});

// 上传/public/css目录下的css文件到oss
gulp.task('oss-css', ['css'], () => {
  const options = {
    accessKeyId: 'LTAIaIAYpvofCzsY',
    accessKeySecret: 'hACXjkAsmwko4qz0E022PZzGyPkhdF',
    region: 'oss-cn-shanghai',
    bucket: 'lilithimage',
    prefix: 'aoc/css'
  };
  return gulp.src('./public/css/*')
    .pipe(oss(options));
});

// 上传/public/js目录下的js文件到oss
gulp.task('oss-js', ['js'], () => {
  const options = {
    accessKeyId: 'LTAIaIAYpvofCzsY',
    accessKeySecret: 'hACXjkAsmwko4qz0E022PZzGyPkhdF',
    region: 'oss-cn-shanghai',
    bucket: 'lilithimage',
    prefix: 'aoc/js'
  };
  return gulp.src('./public/js/*')
    .pipe(oss(options));
});

// 上传/public/vendor目录下的文件到oss
gulp.task('oss-vendor', () => {
  const options = {
    accessKeyId: 'LTAIaIAYpvofCzsY',
    accessKeySecret: 'hACXjkAsmwko4qz0E022PZzGyPkhdF',
    region: 'oss-cn-shanghai',
    bucket: 'lilithimage',
    prefix: 'aoc/vendor'
  };
  return gulp.src('./public/vendor/**/*')
    .pipe(oss(options));
});

gulp.task('css', [...commonCssTasks, ...remCssTasks, ...mergeCssTasks]);

gulp.task('js', [...jsTasks]);

gulp.task('default', [ 'oss-css', 'oss-js', 'oss-vendor' ]);