'use strict';

const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    app: {
      name: 'koa-mvc',
      keys: [ '@sdk^dih&sdf' ],
      timeout: 120000
    },
    port: 3001
  },
  test: {
    app: {
      name: 'koa-mvc',
      keys: [ '@sdk^dih&sdf' ],
      timeout: 120000
    },
    port: 3001
  },
  production: {
    app: {
      name: 'koa-mvc',
      keys: [ '@sdk^dih&sdf' ],
      timeout: 120000
    },
    port: 6231
  },
};

module.exports = config[ env ];